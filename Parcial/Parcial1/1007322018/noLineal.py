import numpy as np
import matplotlib.pyplot as plt

class PenduloNoLineal:

    def __init__(self,u0,du0,f):
        self.u0 = u0
        self.du0 = du0
        self.f = f
        self.a = 0
        self.b = 1.0
        self.N =1000
        self.h = (self.b-self.a)/self.N
        self.T = np.arange(self.a,self.b,self.h)
        self.U = []
        self.Y = []
    
    def RungeKutta(self):

        u = self.u0
        y = self.du0      

        for t in self.T:

         self.U.append(u)
         self.Y.append(y)

         m1 = self.h*y
         k1 = self.h*self.f(u, y, t)  

         m2 = self.h*(y + 0.5*k1)
         k2 = self.h*self.f(u+0.5*m1, y+0.5*k1, t+0.5*self.h)

         m3 = self.h*(y + 0.5*k2)
         k3 = self.h*self.f(u+0.5*m2, y+0.5*k2, t+0.5*self.h)

         m4 = self.h*(y + k3)
         k4 = self.h*self.f(u+m3, y+k3, t+self.h)

         y += (m1 + 2*m2 + 2*m3 + m4)/6
         u += (k1 + 2*k2 + 2*k3 + k4)/6

        return self.U, self.Y

    def DesplazamientoAngular(self):
        
        def F(u,y,t):
           g=-10
           l=1
           return (-g/l)*np.sin(u)

        u = 1
        y = 0      

        for t in self.T:

         self.U.append(u)
         self.Y.append(y)

         m1 = self.h*y
         k1 = self.h*F(u, y, t)  

         m2 = self.h*(y + 0.5*k1)
         k2 = self.h*F(u+0.5*m1, y+0.5*k1, t+0.5*self.h)

         m3 = self.h*(y + 0.5*k2)
         k3 = self.h*F(u+0.5*m2, y+0.5*k2, t+0.5*self.h)

         m4 = self.h*(y + k3)
         k4 = self.h*F(u+m3, y+k3, t+self.h)

         y += (m1 + 2*m2 + 2*m3 + m4)/6
         u += (k1 + 2*k2 + 2*k3 + k4)/6

        return self.U


   



    