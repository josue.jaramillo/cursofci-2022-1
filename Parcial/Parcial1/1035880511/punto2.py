# Considerar el problema de tiro parabólico, con aceleración en los ejes x y y. En el eje y la aceleración de
#la gravedad y en ele eje x una aceleración proporcionada por un viento en contra del movimiento
#código dependiente del tiempo
import numpy as np
import matplotlib.pyplot as plt

class tiroparabolico: 
    #Inicializar atributos de velocidad inicial, ángulo, gravedad, aceleración en x 
    def __init__(self, velocidad_ini, angulo,gravedad,acelx):
        self.velocidad_ini = velocidad_ini
        self.angulo = angulo 
        self.gravedad = gravedad
        self.acelx=acelx
    # método para la velocidad inicial en x
    def vx_ini(self): 
        self.a = self.velocidad_ini*np.cos(self.angulo)
        return self.a
    # método para la velocidad inicial en y 
    def vy_ini(self): 
        self.b = self.velocidad_ini*np.sin(self.angulo)
        return self.b
    # método para el tiempo de vuelo
    def t_vuelo(self):
        self.tv = 2*self.vy_ini()/(self.gravedad)
        return self.tv
    # método para el tiempo
    def tiempo(self):
        self.t = np.arange(0, self.t_vuelo()+0.1 , 0.1)    
        return  self.t
    # # método para la velocidad en x

    def velx(self):
        self.vx= self.acelx * self.tiempo() + self.vx_ini()
        return self.vx
    # método para la velocidad en y
    def vely(self):
        self.vy = -(self.gravedad * self.tiempo()) + self.vy_ini()
        return self.vy
    # método para la posición en x
    def posx(self):
        self.xf =(self.acelx/2)*(self.tiempo())**2 + self.vx_ini() * self.tiempo()
        return self.xf
# método para la posición en y

    def posy(self): 
        self.y = -0.5 *self.gravedad* (self.tiempo())**2 + self.tiempo()*self.vy_ini()
        return self.y

# método para la posición máxima en y

    def y_max(self):
        self.ym = 0.5*self.vy_ini()/self.gravedad
        return self.ym
# método para la posición máxima en x
    def x_max(self):
        self.xm = 0.5*self.vx_ini()/self.gravedad
        return self.xm        

# clase de herencia que utiliza la clase definida como tiroparabólico
class grafica(tiroparabolico):
    def __init__(self, velocidad_ini, angulo,gravedad,acelx):
        tiroparabolico.__init__(self,velocidad_ini, angulo,gravedad,acelx)
    # Se ejecutan las gráficas    
    def graf(self):
 
        posgx = self.posx()
        posgy = self.posy()
        plt.plot(posgx,posgy)
        plt.xlabel("Posición en x")
        plt.ylabel("Posición en y")
        plt.show()


        
        

