
import numpy as np

class particulaCampoMagnetico:

    '''
    Clase con metodos que dan la solucion al problema de una particula en presencia de un
    campo magnetico uniforme, donde se asume la particula en el tiempo inicial en el origen
    del sistema de coordenadas elegido. 

    Parametros:
        Ek: energia cinetica inicial de la particula en unidades de eV
        theta: angulo que forma el vector velocidad con el vector de campo magnetico, unidades de grados
        m: masa de la particula en Kg
        q: carga de la particula en C (coulomb)
        B: valor de la magnitud del campo magnetico

        t: tiempo

    Metodos:
        parametros: 
            t: tiempo en el que se quiere conocer la posicion o velocidad de la particula

        x: retorna la poscion de la particula en x en el tiempo t
        y: retorna la poscion de la particula en y en el tiempo t
        z: retorna la poscion de la particula en z en el tiempo t

        vx: velocidad de la particula en la dirección de x en el tiempo t
        vy: velocidad de la particula en la dirección de y en el tiempo t
        vz: velocidad de la particula en la dirección de z en el tiempo t

    '''

    def __init__(self, Ek, theta, m, q, B):

        #Pasando a Joules la energia cinetica
        self.Ek = Ek*1.60e-19
        #Pasando el angulo a radianes
        self.theta = theta*np.pi/180

        #Resto de parametros ingresarlos en unidades del sistema internacional
        self.m = m  #[kg]
        self.q = q  #[C]
        self.B = B  #[T]

        #velocidad inicial dado la energia cinetica 
        self.v = np.sqrt( 2*self.Ek/self.m )

        #Componentes de la velocidad
        self.vx0 = self.v*np.sin( self.theta)
        self.vz0 = self.v*np.cos( self.theta)


    def x(self, t):
        x = self.m*self.vx0/(self.q*self.B)*np.sin((self.q*self.B/self.m)*t)
        return x

    def y(self, t):
        y = self.m*self.vx0/(self.q*self.B)*( np.cos((self.q*self.B/self.m)*t) - 1 )
        return y

    def z(self, t):
        z = self.vz0*t
        return z
        
    def vx(self,t):
        vx = self.vx0*np.cos( (self.q*self.B/self.m)*t )
        return vx
    
    def vy(self,t):
        vy = -self.vx0*np.sin( (self.q*self.B/self.m)*t )
        return vy
    
    def vz(self,t):
        vz = self.vz0
        return vz