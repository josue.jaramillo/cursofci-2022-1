import numpy as np
import matplotlib.pyplot as plt

#Clase pedida por el profesor para la solución del ejercicio
class TiroParabolico:
    
    #Constructor
    def __init__(self,vel_in,angulo,gravedad,ac_viento,altura):
        self.vi = vel_in                                                #Velocidad inicial 
        self.a = angulo*(np.pi/180)                                     #Ángulo en grados, transformado a radianes
        self.g = gravedad                                               #Aceleración de la gravedad
        self.av = ac_viento                                             #Aceleración del viento 
        self.h = altura                                                 #Altura inicial
    
    #Método para calcular la velocidad en la componente x
    def VelocidadEnX(self):
        vx = self.vi*np.cos(self.a)
        return vx
    
    #Método para calcular la velocidad en la componente y
    def VelocidadEnY(self):
        vy = self.vi*np.sin(self.a)
        return vy
    
    #Método para calcular el tiempo de vuelo de la esfera hasta que llega al suelo
    def TiempoDeVuelo(self):
        tv = (self.VelocidadEnY() + np.sqrt(self.VelocidadEnY()**2 + 2*self.g*self.h))/self.g
        return tv
    
    #Método para calcular el alcance máximo en x de la esfera
    def AlcanceMax(self):
        xmax = self.VelocidadEnX()*self.TiempoDeVuelo() + (1/2)*self.av*self.TiempoDeVuelo()**2
        return xmax
    
    #Método para calcular la altura máxima en y de la esfera
    def AlturaMax(self):
        ymax = self.h + (self.VelocidadEnY()**2)/(2*self.g) 
        return ymax
    
#Clase que hereda los atributos de la anteriormente pedida
class PosicionYGrafica(TiroParabolico):
    
    #Herencia
    def __init__(self,vel_in,angulo,gravedad,ac_viento,altura):
        TiroParabolico.__init__(self,vel_in,angulo,gravedad,ac_viento,altura)
    
    #Se genera un arreglo de tiempo para la graficación
    def ArregloTiempo(self):
        #Se calcula el tiempo de vuelo y posteriormente se crea un arreglo de numpy
        tv = (self.vi*np.sin(self.a) + np.sqrt((self.vi*np.sin(self.a))**2 + 2*self.g*self.h))/self.g
        t = np.arange(0,abs(tv)+abs(tv)*0.005,abs(tv)*0.005)
        return t
        
    #Se calcula un arreglo de posiciones en el eje x
    def PosicionEnX(self):         
        x = self.vi*np.cos(self.a)*self.ArregloTiempo() - (1/2)*self.av*self.ArregloTiempo()**2
        return x
    
    #Se calcula un arreglo de posiciones en el eje y
    def PosicionEnY(self):
        y = self.h + self.vi*np.sin(self.a)*self.ArregloTiempo() - (1/2)*self.g*self.ArregloTiempo()**2
        return y
    
    #Se grafican ambos arreglos previamente calculados
    def Grafica(self):
        plt.plot(self.PosicionEnX(),self.PosicionEnY())
        plt.title('Movimiento Parabolico')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.grid(0.2)
        plt.savefig('TiroParabolico.png')
        plt.show()
